/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  createAppContainer,
} from "react-navigation";

import { createStackNavigator } from 'react-navigation-stack';
import * as AppConstants from "./SRC/Component/Helper/AppConstants";
import { Platform, StyleSheet, Text, View, TouchableOpacity, TouchableHighlight } from 'react-native';
import Mainpage from './SRC/Component/MainPage/Mainpage';
import ColorStrip from './SRC/Component/ColorStrip/ColorStrip'
const Stack = createStackNavigator(
  {
    Mainpage: {
      screen: Mainpage,
      params: {
        header: false
      },
      navigationOptions: {
        header: null
      }
    },
    ColorStrip: {
      screen: ColorStrip,
      params: {
        header: false
      },
      navigationOptions: {
        header: null
      }
    },
  }, {
    mode: "card",
    headerMode: "screen",
    // cardStyle: { backgroundColor: '#f9f8ff' },
    defaultNavigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
      headerLeft: (
        getHeaderLeft(navigation)
      ),
      layout: {
        orientation: ['portrait']
      },
      // headerRight: (getHedarRight(navigation)),
      headerBackground: getHeaderBackground(navigation),
      headerStyle: {
        elevation: 0,
        borderBottomWidth: 0,
        ...Platform.select({
          android: {
            height: AppConstants.getDeviceHeight(13),
          },
  
        })
      },
  
      headerTitleStyle: {
        color: AppConstants.COLORS.PROFILEINACTIVE,
  
        fontSize: AppConstants.FONTSIZE.FS18,
        fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
        justifyContent: 'center',
        fontWeight: 'bold',
        alignSelf: 'center',
        ...Platform.select({
          android: {
            justifyContent: 'center',
            alignSelf: 'center',
            textAlign: 'center',
            fontSize: AppConstants.FONTSIZE.FS18,
            fontFamily: AppConstants.FONTFAMILY.fontFamily_2,
            width: AppConstants.getDeviceWidth(60),
            marginTop: AppConstants.getDeviceWidth(5)
          }
        })
      },
    }),
  },
    {
      headerMode: 'screen',
      cardStyle: { backgroundColor: AppConstants.COLORS.VIEWBACKGROUND },
    },
    {
      headerMode: 'none',
      initialRouteName: "BankingCalculator"
    }
  )
  function getHeaderLeft(navigation) {
    switch (navigation.state.routeName) {
      case "Tab":
        return null;
      default:
        return AppConstants.BackButton(navigation)
    }
  }
  function getHeaderBackground(navigation) {
    return (
      <View style={{ flex: 1, backgroundColor: AppConstants.COLORS.APPCOLOR, justifyContent: 'center', }}>
      </View>
    );
  }
  const AppContainer = createAppContainer(Stack);
  
  export default class App extends Component {
    render() {
      return <AppContainer />;
    }
  }
  