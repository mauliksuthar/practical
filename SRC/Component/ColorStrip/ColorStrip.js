
import React, { Component } from 'react';
import { View, Text, StatusBar, FlatList, Alert, TouchableOpacity, SectionList, ActivityIndicator, Image, ImageBackground, ScrollView, } from 'react-native';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import * as AppConstants from '../Helper/AppConstants';
import styles from './ColorStripStyle';
const Images = {
    Back: 'back'
}
export default class ColorStrip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subcategoryid: this.props.navigation.state.params.subcategoryid,
            one: '#FF5733',
            two: '#0EEE26',
            three: '#90C6AF',
            four: '#A0C690',
            noone: '0',
            notwo: '0',
            nothree: '0',
            nofour: '0',
        };
        this.sectoinheader = this.sectoinheader.bind(this)
        this._renderRow1 = this._renderRow1.bind(this);
        this._renderRow2 = this._renderRow2.bind(this);

    }
    componentDidMount() {

    }
    _renderRow2({ item, index }) {
        console.log('enter', item)
        return (
            <TouchableOpacity onPress={() => {
                if (item.index == 0) {
                    this.setState({ one: item.color,noone:item.no })
                }
                else if (item.index == 1) {
                    this.setState({ two: item.color,notwo:item.no })
                }
                else if (item.index == 2) {
                    this.setState({ three: item.color ,nothree:item.no})
                }
                else if (item.index == 3) {
                    this.setState({ four: item.color,nofour:item.no })
                }
            }}>
                <View style={[styles.colorviewselct, { backgroundColor: item.color }]}></View>
                <Text style={{ alignSelf: 'center', marginTop: AppConstants.getDeviceHeight(1) }}>{item.no}</Text>
            </TouchableOpacity>
        )
    }
    _renderRow1({ section, index, }) {
        var datas = index;
        return (
            // <View></View>
            datas == 0 ? <View><FlatList
                style={{ marginTop: AppConstants.getDeviceHeight(2) }}
                horizontal={true}
                data={section.data}
                renderItem={this._renderRow2.bind(this)}
                keyExtractor={(item, index) => index.toString()}
            /></View> : <View></View>
        )
    }
    sectoinheader({ section }) {
        return (
            <View style={{
                marginTop: AppConstants.getDeviceHeight(2),
                flexDirection: 'row',
            }}>
                <Text style={styles.texthedar}>{section.title}</Text>
                <View style={styles.viewcolor}>
                    <Text style={{ alignSelf: 'center' }}>{section.index==this.state.one?this.state.noone:'0'}</Text>
                </View>
            </View>
        )
    }
    render() {
        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundColor={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>

                <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                    <Image style={styles.backimage} source={{ uri: Images.Back }}></Image>

                </TouchableOpacity>
                <Text style={styles.COLORStriop}> ColorStrip </Text>
                <View style={{ width: AppConstants.getDeviceWidth(100), flexDirection: 'row', }}>
                    <View style={styles.linewview}>
                        <View style={[styles.colorview1, { backgroundColor: this.state.one }]}></View>
                        <View style={[styles.colorview2, { backgroundColor: this.state.two }]}></View>
                        <View style={[styles.colorview3, { backgroundColor: this.state.three }]}></View>
                        <View style={[styles.colorview4, { backgroundColor: this.state.four }]}></View>
                    </View>
                    <SectionList
                        sections={AppConstants.array}
                        style={{ width: AppConstants.getDeviceWidth(4) }}
                        renderSectionHeader={this.sectoinheader.bind(this)}
                        keyExtractor={(item, index) => index}
                        renderItem={this._renderRow1.bind(this)}
                    />
                </View>

            </View >
        );
    }
}
