import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,

    },
    backimage: {
        height: AppConstants.getDeviceHeight(3),
        width: AppConstants.getDeviceWidth(3),
        marginTop: AppConstants.getDeviceHeight(5),
        marginLeft: AppConstants.getDeviceWidth(5)
    },
    COLORStriop: {
        fontSize: AppConstants.FONTSIZE.FS18,
        color: 'blue',
        marginTop: AppConstants.getDeviceHeight(5),
        marginLeft: AppConstants.getDeviceWidth(5),
        fontWeight: 'bold'
    },
    linewview :{
        width: AppConstants.getDeviceWidth(5),
        height: AppConstants.getDeviceHeight(80),
        borderColor: AppConstants.COLORS.HEDARBACKGROUND,
        borderRadius: 10,
        borderWidth: 1,
        marginLeft: AppConstants.getDeviceWidth(4),
        marginTop: AppConstants.getDeviceHeight(2),
    },
    texthedar:{
        alignSelf: 'center',
        justifyContent: 'center',
        marginLeft: AppConstants.getDeviceWidth(4),
        marginTop: AppConstants.getDeviceHeight(2)
    },
    viewcolor:{
        width: AppConstants.getDeviceWidth(20),
        height: AppConstants.getDeviceHeight(5),
        borderColor: AppConstants.COLORS.HEDARBACKGROUND,
        borderRadius: 10,
        borderWidth: 1,
        position: 'absolute',
        right: 0,
        marginRight: AppConstants.getDeviceWidth(5),
        justifyContent: 'center',
    },
    colorviewselct:{
        width: AppConstants.getDeviceWidth(15),
        height: AppConstants.getDeviceHeight(4),
        borderRadius: 5,
        marginLeft:AppConstants.getDeviceWidth(2),
      
    },
    colorview1:{
        width: AppConstants.getDeviceWidth(5),
        height: AppConstants.getDeviceHeight(4),
       
        marginTop:AppConstants.getDeviceHeight(6.5)
    },
    colorview2:{
        width: AppConstants.getDeviceWidth(5),
        height: AppConstants.getDeviceHeight(4),
      
        marginTop:AppConstants.getDeviceHeight(12)
    },
    colorview3:{
        width: AppConstants.getDeviceWidth(5),
        height: AppConstants.getDeviceHeight(4),
        
        marginTop:AppConstants.getDeviceHeight(12)
    },
    colorview4 :{
        width: AppConstants.getDeviceWidth(5),
        height: AppConstants.getDeviceHeight(4),
       
        marginTop:AppConstants.getDeviceHeight(12.5)
    }
}