import React, { Component } from 'react';
import { View, Text, StatusBar, FlatList, Alert, TouchableOpacity, SectionList, ActivityIndicator, Image, ImageBackground, ScrollView, } from 'react-native';
import * as AppConstants from '../Helper/AppConstants';
import styles from './MainpageStyle';
import API from "../WebServices/API";
import { PostData, internet } from '../WebServices/webAPIRequest';
import OfflineNotice from '../Helper/OfflineNotice';
import ExpanableList from 'react-native-expandable-section-flatlist';
import Iconss from "react-native-vector-icons/Feather";
const Images = {
    filter: 'filter',
    search: 'search'
}
export default class Mainpage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            DataSource: [],
            SubCategory: [],
            selectedvalue: '',
            isLoading: false
        };
        this._renderRow = this._renderRow.bind(this);
        this.sectoinheader = this.sectoinheader.bind(this)
        this._renderRow1 = this._renderRow1.bind(this);
        this._renderRow2 = this._renderRow2.bind(this);

    }
    componentDidMount() {
        // this.Selctedtitem(59);
        this.ViewDetail();
    }
    ViewDetail() {
        this.setState({ isLoading: true });
        APIData = JSON.stringify({
            CategoryId: 0,
            DeviceManufacturer: "Google",
            DeviceModel: "Android SDK built for x86",
            DeviceToken: " ",
            PageIndex: 1
        });
        console.log(APIData)
        PostData(API.Dashboard, APIData)
            .then((responseJson) => {
                console.log(responseJson)
                this.setState({ isLoading: false });
                if (responseJson.Status == 200) {
                    this.setState({ DataSource: responseJson.Result.Category, selectedvalue: responseJson.Result.Category[0].Name })
                    this.Selctedtitem(responseJson.Result.Category[0].Id)
                }
                else {
                    Alert.alert(responseJson.msg);
                }
            }).catch(() => {
                this.setState({ isLoading: false });
                Alert.alert(AppConstants.Messages.APIERROR);
            });


    }
    Selctedtitem(id) {
        this.setState({ isLoading: true });
        APIData = JSON.stringify({
            CategoryId: id,
            PageIndex: 2
        });
        console.log(APIData)
        PostData(API.Dashboard, APIData)
            .then((responseJson) => {
                console.log(responseJson.Result.Category[0].SubCategories)
                this.setState({ isLoading: false });
                if (responseJson.Status == 200) {
                    var arry = [];
                    responseJson.Result.Category[0].SubCategories.map((element) => {
                        arry.push({
                            title: element.Name,
                            data: element.Product
                        });
                    }
                    );
                    this.setState({ SubCategory: arry, selectedvalue: responseJson.Result.Category[0].Name })
                }
                else {
                    Alert.alert(responseJson.msg);
                }
            }).catch(() => {
                this.setState({ isLoading: false });
                Alert.alert(AppConstants.Messages.APIERROR);
            });


    }

    _renderRow({ item, index }) {
        return (
            <TouchableOpacity onPress={() => {
                this.setState({ selectedvalue: item.Name }),
                    this.Selctedtitem(item.Id)
            }}
                style={styles.textview}>
                <Text
                    style={[{
                        color: this.state.selectedvalue == item.Name ? AppConstants.COLORS.WHITE : AppConstants.COLORS.datecolor,
                    }, styles.textname]}>{item.Name}</Text>
            </TouchableOpacity>
        )
    }
    _renderRow2({ item, index }) {
        return (
            <TouchableOpacity onPress={() => { this.props.navigation.navigate(AppConstants.SCREENS.COLORSTRIP, { subcategoryid: item.Id }) }}>
                <View style={styles.imageview}>
                    <ImageBackground style={styles.ijmagestyle} source={{ uri: item.ImageName }}></ImageBackground>
                    <Text style={styles.tilename}>{item.Name}</Text>
                </View>
                <View style={styles.priceview}>
                    <Text style={styles.pricetext}>{item.PriceCode}</Text>

                </View>
            </TouchableOpacity>

        )
    }
    _renderRow1({ section, index, }) {
        var datas = index;
        return (
            datas == 0 ? <View><FlatList
                style={{ marginTop: AppConstants.getDeviceHeight(2) }}
                horizontal={true}
                data={section.data}
                renderItem={this._renderRow2.bind(this)}
                keyExtractor={(item, index) => index.toString()}
            /></View> : <View></View>
        )
    }
    sectoinheader({ section }) {
        return (
            <View style={{ marginTop: AppConstants.getDeviceHeight(2) }}>
                <Text style={styles.hedartext}>{section.title}</Text>
            </View>
        )
    }
    render() {

        return (
            <View style={styles.mainContainer}>
                <StatusBar barStyle="light-content" backgroundColor={AppConstants.SHADOWCOLORS.INTRO} animated translucent></StatusBar>
                <View style={styles.hedarview}>


                    <View>
                        <FlatList
                            style={{ marginTop: AppConstants.getDeviceHeight(10) }}
                            horizontal={true}
                            data={this.state.DataSource}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={this._renderRow.bind(this)}
                        />
                    </View>
                </View>
                <View style={styles.sectoinlistview}>
                    <SectionList
                        sections={this.state.SubCategory}
                        renderSectionHeader={this.sectoinheader.bind(this)}
                        keyExtractor={(item, index) => index}
                        renderItem={this._renderRow1.bind(this)}
                    />
                </View>
                {this.state.isLoading ? <View style={AppConstants.CommonStyles.spinner}>
                    <View style={AppConstants.CommonStyles.spinner1}>
                        <ActivityIndicator color='white' size='large' ></ActivityIndicator>
                        <Text style={AppConstants.CommonStyles.spinertext}>{AppConstants.Messages.SPINERMESSAGE}</Text>
                    </View>
                </View> : null}
            </View>
        );
    }
}
