import * as AppConstants from '../Helper/AppConstants';
export default {
    mainContainer: {
        flex: 1,
        backgroundColor: 'black',
    },
    hedarview: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(15),
        backgroundColor: 'black',
    },
    textname: {
        fontSize: AppConstants.moderateScale(AppConstants.FONTSIZE.FS14),
        fontWeight: 'bold',
    },
    textview: {
        marginLeft: AppConstants.getDeviceWidth(4)
    },
    imageview: {
        width: AppConstants.getDeviceWidth(27),
        marginLeft: AppConstants.getDeviceWidth(3),
        marginBottom: AppConstants.getDeviceHeight(2),
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
        overflow: 'hidden',
        justifyContent: 'center',
    },
    ijmagestyle: {
        width: AppConstants.getDeviceWidth(27),
        height: AppConstants.getDeviceWidth(27),

    },
    priceview: {
        width: AppConstants.getDeviceWidth(10),
        height: AppConstants.getDeviceHeight(3),
        backgroundColor: AppConstants.COLORS.FACEBOOK,
        borderRadius: 5,
        position: 'absolute',
        marginLeft: AppConstants.getDeviceHeight(3),
        marginTop: AppConstants.getDeviceHeight(1)
    },
    pricetext: {
        alignSelf: 'center',
        color: 'white'
    },
    tilename: {
        alignSelf: 'center',
        marginTop: AppConstants.getDeviceHeight(2)
    },
    hedartext: {
        fontSize: AppConstants.FONTSIZE.FS16,
        marginLeft: AppConstants.getDeviceWidth(3)
    },
    sectoinlistview: {
        width: AppConstants.getDeviceWidth(100),
        height: AppConstants.getDeviceHeight(100),
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        backgroundColor: AppConstants.COLORS.WHITE
    }
}